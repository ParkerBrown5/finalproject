package edu.towson.cosc431.brown.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.gson.Gson

class SearchButtonActivity : AppCompatActivity() {
    private lateinit var submitButton:Button
    private lateinit var carbsText: EditText
    private lateinit var fatsText: EditText
    private lateinit var proteinText: EditText
    private lateinit var nameText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_screen)
        grabWidgets()
        submitButton.setOnClickListener { saveFood() }
    }
    fun grabWidgets(){
        submitButton=findViewById(R.id.SubmitNewFood)
        carbsText=findViewById(R.id.ManualEditCarbs)
        fatsText=findViewById(R.id.ManualEditFats)
        proteinText=findViewById(R.id.ManualEditProtein)
        nameText=findViewById(R.id.ManualEditName)
    }
    fun saveFood(){
        val newFood=Food(
            name = nameText.editableText.toString(),
            carbs = carbsText.editableText.toString().toInt(),
            fats = fatsText.editableText.toString().toInt(),
            protein = proteinText.editableText.toString().toInt()
        )
        val intent= Intent()

        val gson= Gson()
        val json=gson.toJson(newFood)
        intent.putExtra(FOOD_KEY, json)
        setResult(RESULT_OK, intent)
        finish()
    }
    companion object{
        const val FOOD_KEY="food_key"
    }
}
package edu.towson.cosc431.brown.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private lateinit var startButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        grabWidgets()
        startButton.setOnClickListener { startActivity() }
    }
    fun grabWidgets(){
        startButton=findViewById(R.id.start_button)
    }
    fun startActivity(){
        val intent= Intent(this, MainScreen::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }
    companion object{
        const val REQUEST_CODE=1
    }

}
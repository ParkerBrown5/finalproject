package edu.towson.cosc431.brown.myapplication

class FoodRepository {
    private val foods: MutableList <Food> = mutableListOf()
    private var carbs=0
    private var fats=0
    private var protein=0
    private var calories=0



    fun addFood( food: Food){
        carbs=carbs+food.carbs
        fats=fats+food.fats
        protein=protein+food.protein
        calories=calories+food.fats*9+food.carbs*4+food.protein*4
        foods.add(food)
    }
    fun getFood(idx: Int): Food{
        return foods[idx]
    }
    fun deleteFood(idx: Int){
        val dummy=foods[idx]
        carbs=carbs-dummy.carbs
        fats=fats-dummy.fats
        protein=protein-dummy.protein
        calories=calories-dummy.protein*4-dummy.carbs*4-dummy.fats*9

    }
    fun getCarbs(): Int{
        return carbs
    }
    fun getFats(): Int{
        return fats
    }
    fun getProtein(): Int{
        return protein
    }
    fun getCalories(): Int{
        return calories
    }
}
package edu.towson.cosc431.brown.myapplication

data class Food(
    val name: String,
    val carbs: Int,
    val fats: Int,
    val protein: Int
)
package edu.towson.cosc431.brown.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.gson.Gson

class MainScreen : AppCompatActivity() {
    private lateinit var foodRepository: FoodRepository
    private lateinit var searchButton: Button
    private lateinit var backButton:Button
    private lateinit var carbsText: TextView
    private lateinit var fatsText: TextView
    private lateinit var proteinText: TextView
    private lateinit var caloriesText: TextView
    private lateinit var displayButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_screen)
        foodRepository= FoodRepository()
        grabWidgets()
        searchButton.setOnClickListener { launchSearchActivity() }
        backButton.setOnClickListener { goBack() }
        displayButton.setOnClickListener { display() }
        update()
    }
    fun grabWidgets(){
        searchButton=findViewById(R.id.SearchButton)
        backButton=findViewById(R.id.BackButton)
        carbsText=findViewById(R.id.CarbsText)
        fatsText=findViewById(R.id.FatsText)
        proteinText=findViewById(R.id.ProteinText)
        caloriesText=findViewById(R.id.CaloriesText)
        displayButton=findViewById(R.id.ListViewButton)
    }
    fun launchSearchActivity(){
        val intent= Intent(this, SearchButtonActivity::class.java)
        startActivityForResult(intent, REQUEST_CODET)
    }

    fun goBack(){
        finish()
    }
    fun update(){
        carbsText.text= foodRepository.getCarbs().toString()
        fatsText.text=foodRepository.getFats().toString()
        caloriesText.text=foodRepository.getCalories().toString()
        proteinText.text=foodRepository.getProtein().toString()


    }
    fun display(){
        val intent= Intent(this, FoodDisplay::class.java)
        startActivityForResult(intent, REQUEST_CODETH)
    }
    companion object{
        const val REQUEST_CODET=2
        const val REQUEST_CODETH=3
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_CODET ->{
                when(resultCode){
                    RESULT_OK->{
                        val json= data?.getStringExtra(SearchButtonActivity.FOOD_KEY)
                        if(json!=null){
                            val newFood= Gson().fromJson<Food>(json, Food::class.java)
                            foodRepository.addFood(newFood)
                            update()
                        }

                    }
                }
            }
        }
    }
}